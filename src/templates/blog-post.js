import React from 'react'
import Helmet from 'react-helmet'
import get from 'lodash/get'
import Img from 'gatsby-image'


class BlogPostTemplate extends React.Component {
  render() {
    const article = get(this.props, 'data.contentfulArticle')
    const siteTitle = get(this.props, 'data.site.siteMetadata.title')

    return (
      <div style={{ background: '#fff' }}>
        <div className="wrapper">
          <h1 className="section-headline">{article.title}</h1>
          <p
            style={{
              display: 'block',
            }}
          >
            {article.summary}
          </p>
        </div>
      </div>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query ArticleByTitle($title: String!) {
    contentfulArticle(title: { eq: $title }) {
      title
      summary
    }
  }
`
