import React from 'react'
import Link from 'gatsby-link'

export default ({ article }) => (
  <div>
    <h3>
      <Link to={`/blog/${article.slug}`}>{article.title}</Link>
    </h3>
    <p>{article.summary}</p>
  </div>
)
